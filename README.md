# ElisaApp

interviewing application for Elisa mobile developer job 

## Instructions

Android application

* Architecture and testability are appreciated.
* Implementation language: Kotlin
* Application should:
    * Retrieve information (temperature, game results, etc.) from internet.
    * The user triggers the retrieval after the app is launched
    * The user has possibility to input the used URL for retrieval.
* We expect unit testing.
* There should be an abstraction for network.
* It should be possible to variate (runtime/compile time) the network usage.

### Installing

Download link for the apk: https://drive.google.com/open?id=1LLBeynvLhL7LcoZFosOsxZJXAwyZE8g-


### What the app does

Application lists stock information from https://iextrading.com/

Default path is stock/market/list/gainers. You can try out e.g. /losers

User can configure the url path, read timeout, and connect timeout from settings.

The stock list can be updated with swipe to refresh. 

### How the app is created

The app is written in Kotlin. For networking retrofit2 is used for HTTP, gson for converting JSON to objects, and rxjava2 for asynchronously updating the view with the synced data. 
