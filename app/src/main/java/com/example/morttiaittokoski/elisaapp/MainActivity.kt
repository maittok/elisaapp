package com.example.morttiaittokoski.elisaapp

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.internal.schedulers.IoScheduler
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v4.widget.SwipeRefreshLayout

class MainActivity : AppCompatActivity() {

    lateinit private var swipeContainer: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        PreferenceService.init(this)
        PreferenceService.setDefaultPreferences(this)

        // Lookup the swipe container view
        swipeContainer = findViewById(R.id.swipe_container)
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener({updateStocks()})

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light)

        rv_list_stocks.layoutManager = LinearLayoutManager(this)

        updateStocks()
    }

    fun updateStocks() {
        SyncService.getStocks(this).observeOn(AndroidSchedulers.mainThread()).subscribeOn(IoScheduler()).subscribe (
                { retrievedStocks ->
                    rv_list_stocks.adapter = StockItemAdapter(retrievedStocks, this)
                    swipeContainer.isRefreshing = false
                },
                { e ->
                    Snackbar.make(
                            findViewById(R.id.main_layout),
                            e.localizedMessage,
                            Snackbar.LENGTH_LONG
                    ).show()
                    swipeContainer.isRefreshing = false
                })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.action_settings -> {
                openSettings()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun openSettings() {
        val intent = Intent(this, SettingsActivity::class.java).apply {}
        startActivity(intent)
    }
}
