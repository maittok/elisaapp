package com.example.morttiaittokoski.elisaapp.Model
import com.google.gson.annotations.SerializedName

data class Stock(
        @SerializedName("symbol") val symbol: String,
        @SerializedName("companyName") val companyName: String,
        @SerializedName("latestPrice") val latestPrice: Double,
        @SerializedName("changePercent") val changePercent: Double
)