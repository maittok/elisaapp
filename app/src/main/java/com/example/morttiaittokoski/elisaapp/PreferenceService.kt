package com.example.morttiaittokoski.elisaapp

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

object PreferenceService: PreferenceServiceInterface {
    lateinit var defaultPreferences:SharedPreferences

    fun init(context: Context) {
        defaultPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    }

    override fun getReadTimeout(context: Context): Int {
        return Integer.parseInt(defaultPreferences.getString("read_timeout","10"))
    }

    override fun getConnectTimeout(context: Context): Int {
        return Integer.parseInt(defaultPreferences.getString("connect_timeout","10"))
    }

    override fun getSyncUrl(context: Context): String {
        return defaultPreferences.getString("sync_url", "")
    }

    override fun setDefaultPreferences(context: Context) {
        PreferenceManager.setDefaultValues(context, R.xml.default_preferences, false)
    }
}