package com.example.morttiaittokoski.elisaapp

import android.content.Context

interface PreferenceServiceInterface {
    fun getReadTimeout(context: Context): Int

    fun getConnectTimeout(context: Context): Int

    fun getSyncUrl(context: Context): String

    fun setDefaultPreferences(context: Context)
}