package com.example.morttiaittokoski.elisaapp

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.morttiaittokoski.elisaapp.Model.Stock
import kotlinx.android.synthetic.main.stock_item_layout.view.*

class StockItemAdapter(val stockList: List<Stock>, val context: Context) :
        RecyclerView.Adapter<StockItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.stock_item_layout,
                parent, false))
    }

    override fun getItemCount(): Int {
        return stockList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        // set stock symbol
        holder.itemView.txt_stock_symbol.text = stockList.get(position).symbol

        // set stock price
        val price = stockList.get(position).latestPrice
        holder.itemView.txt_stock_price.text = "Price: $price"

        // set company name
        holder.itemView.txt_stock_company.text = stockList.get(position).companyName

        // set change percent, positive change is colored green, negative change is colored red
        val change = stockList.get(position).changePercent
        val changePercent = "%.2f".format(change * 100)
        holder.itemView.txt_stock_change_percent.text = "Change: $changePercent%"
        if (change > 0) {
            holder.itemView.txt_stock_change_percent.setTextColor(Color.parseColor("#008000"))
        } else {
            holder.itemView.txt_stock_change_percent.setTextColor(Color.parseColor("#ff0000"))
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
