package com.example.morttiaittokoski.elisaapp

import com.example.morttiaittokoski.elisaapp.Model.Stock
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Url

interface StockNetworkAPI {

    @GET
    fun getStocks(@Url url: String): Observable<List<Stock>>
}

