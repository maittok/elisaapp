package com.example.morttiaittokoski.elisaapp

import android.content.Context
import com.example.morttiaittokoski.elisaapp.Model.Stock
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object SyncService {
    private const val baseUrl = "https://api.iextrading.com/1.0/"
    var preferenceService: PreferenceServiceInterface = PreferenceService

    fun getStocks(context: Context): Observable<List<Stock>> {
        val okHttpClient = OkHttpClient.Builder()
                .readTimeout(preferenceService.getReadTimeout(context).toLong(), TimeUnit.MILLISECONDS)
                .connectTimeout(preferenceService.getConnectTimeout(context).toLong(), TimeUnit.MILLISECONDS)
                .build()

        val retrofit = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .baseUrl(baseUrl).build()

        val postsApi = retrofit.create(StockNetworkAPI::class.java)

        return postsApi.getStocks(preferenceService.getSyncUrl(context))
    }
}