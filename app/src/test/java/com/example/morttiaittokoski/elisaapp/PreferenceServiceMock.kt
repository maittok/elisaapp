package com.example.morttiaittokoski.elisaapp

import android.content.Context

class PreferenceServiceMock: PreferenceServiceInterface {
    override fun getReadTimeout(context: Context): Int {
        return 1000
    }

    override fun getConnectTimeout(context: Context): Int {
        return 1000
    }

    override fun getSyncUrl(context: Context): String {
        return "https://api.iextrading.com/1.0/"
    }

    override fun setDefaultPreferences(context: Context) {
    }
}