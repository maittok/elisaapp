package com.example.morttiaittokoski.elisaapp

import com.example.morttiaittokoski.elisaapp.Model.Stock
import io.reactivex.Observable
import retrofit2.http.Url


class StockRepoMock(val networkApi : StockNetworkAPI) {

    fun getStocks(@Url url: String): Observable<List<Stock>> {
            return networkApi.getStocks(url)
    }
 }
