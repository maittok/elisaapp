package com.example.morttiaittokoski.elisaapp

import android.content.Context
import android.content.SharedPreferences
import org.junit.Assert.*
import com.example.morttiaittokoski.elisaapp.Model.Stock
import io.reactivex.observers.TestObserver
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Test
import org.mockito.Mockito
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class SyncServiceTests {

    val mockJson = "[{\"symbol\":\"CMTA\",\"companyName\":\"Clementia Pharmaceuticals Inc.\",\"primaryExchange\":\"Nasdaq Global Select\",\"sector\":\"Healthcare\",\"calculationPrice\":\"close\",\"open\":13.01,\"openTime\":1540560600839,\"close\":14.03,\"closeTime\":1540584000316,\"high\":15.15,\"low\":12.41,\"latestPrice\":14.03,\"latestSource\":\"Close\",\"latestTime\":\"October 26, 2018\",\"latestUpdate\":1540584000316,\"latestVolume\":92444,\"iexRealtimePrice\":null,\"iexRealtimeSize\":null,\"iexLastUpdated\":null,\"delayedPrice\":14.03,\"delayedPriceTime\":1540584000316,\"extendedPrice\":13.71,\"extendedChange\":-0.32,\"extendedChangePercent\":-0.02281,\"extendedPriceTime\":1540585429421,\"previousClose\":13.19,\"change\":0.84,\"changePercent\":0.06368,\"iexMarketPercent\":null,\"iexVolume\":null,\"avgTotalVolume\":98998,\"iexBidPrice\":null,\"iexBidSize\":null,\"iexAskPrice\":null,\"iexAskSize\":null,\"marketCap\":444997704,\"peRatio\":-4.74,\"week52High\":20.15,\"week52Low\":8.1001,\"ytdChange\":-0.1986754153522608},{\"symbol\":\"TRVG\",\"companyName\":\"trivago N.V.\",\"primaryExchange\":\"Nasdaq Global Select\",\"sector\":\"Consumer Cyclical\",\"calculationPrice\":\"close\",\"open\":5.84,\"openTime\":1540560600344,\"close\":6.55,\"closeTime\":1540584000426,\"high\":6.64,\"low\":5.767,\"latestPrice\":6.55,\"latestSource\":\"Close\",\"latestTime\":\"October 26, 2018\",\"latestUpdate\":1540584000426,\"latestVolume\":2021874,\"iexRealtimePrice\":null,\"iexRealtimeSize\":null,\"iexLastUpdated\":null,\"delayedPrice\":6.55,\"delayedPriceTime\":1540584000426,\"extendedPrice\":6.55,\"extendedChange\":0,\"extendedChangePercent\":0,\"extendedPriceTime\":1540586186238,\"previousClose\":6.2,\"change\":0.35,\"changePercent\":0.05645,\"iexMarketPercent\":null,\"iexVolume\":null,\"avgTotalVolume\":1157851,\"iexBidPrice\":null,\"iexBidSize\":null,\"iexAskPrice\":null,\"iexAskSize\":null,\"marketCap\":2298035202,\"peRatio\":-46.79,\"week52High\":10.05,\"week52Low\":4.01,\"ytdChange\":-0.009170542082738944},{\"symbol\":\"EIDX\",\"companyName\":\"Eidos Therapeutics Inc.\",\"primaryExchange\":\"Nasdaq Global Select\",\"sector\":\"Healthcare\",\"calculationPrice\":\"close\",\"open\":11.61,\"openTime\":1540560600576,\"close\":12.41,\"closeTime\":1540584000461,\"high\":12.7,\"low\":10.07,\"latestPrice\":12.41,\"latestSource\":\"Close\",\"latestTime\":\"October 26, 2018\",\"latestUpdate\":1540584000461,\"latestVolume\":206562,\"iexRealtimePrice\":null,\"iexRealtimeSize\":null,\"iexLastUpdated\":null,\"delayedPrice\":12.41,\"delayedPriceTime\":1540584000461,\"extendedPrice\":12.41,\"extendedChange\":0,\"extendedChangePercent\":0,\"extendedPriceTime\":1540586436760,\"previousClose\":11.93,\"change\":0.48,\"changePercent\":0.04023,\"iexMarketPercent\":null,\"iexVolume\":null,\"avgTotalVolume\":150825,\"iexBidPrice\":null,\"iexBidSize\":null,\"iexAskPrice\":null,\"iexAskSize\":null,\"marketCap\":456092655,\"peRatio\":null,\"week52High\":24.75,\"week52Low\":8.89,\"ytdChange\":-0.4230052941176471},{\"symbol\":\"LRN\",\"companyName\":\"K12 Inc\",\"primaryExchange\":\"New York Stock Exchange\",\"sector\":\"Consumer Defensive\",\"calculationPrice\":\"close\",\"open\":20.85,\"openTime\":1540560600556,\"close\":21.79,\"closeTime\":1540584122381,\"high\":22.24,\"low\":20.57,\"latestPrice\":21.79,\"latestSource\":\"Close\",\"latestTime\":\"October 26, 2018\",\"latestUpdate\":1540584122381,\"latestVolume\":611232,\"iexRealtimePrice\":null,\"iexRealtimeSize\":null,\"iexLastUpdated\":null,\"delayedPrice\":21.79,\"delayedPriceTime\":1540584122381,\"extendedPrice\":21.72,\"extendedChange\":-0.07,\"extendedChangePercent\":-0.00321,\"extendedPriceTime\":1540586594946,\"previousClose\":20.75,\"change\":1.04,\"changePercent\":0.05012,\"iexMarketPercent\":null,\"iexVolume\":null,\"avgTotalVolume\":323512,\"iexBidPrice\":null,\"iexBidSize\":null,\"iexAskPrice\":null,\"iexAskSize\":null,\"marketCap\":875817760,\"peRatio\":41.11,\"week52High\":24.6751,\"week52Low\":12.72,\"ytdChange\":0.40269603972687773},{\"symbol\":\"TAL\",\"companyName\":\"TAL Education Group American Depositary Shares\",\"primaryExchange\":\"New York Stock Exchange\",\"sector\":\"Consumer Defensive\",\"calculationPrice\":\"close\",\"open\":25,\"openTime\":1540560613075,\"close\":27.9,\"closeTime\":1540584081375,\"high\":27.9,\"low\":24.77,\"latestPrice\":27.9,\"latestSource\":\"Close\",\"latestTime\":\"October 26, 2018\",\"latestUpdate\":1540584081375,\"latestVolume\":14239031,\"iexRealtimePrice\":null,\"iexRealtimeSize\":null,\"iexLastUpdated\":null,\"delayedPrice\":27.9,\"delayedPriceTime\":1540584081375,\"extendedPrice\":27.94,\"extendedChange\":0.04,\"extendedChangePercent\":0.00143,\"extendedPriceTime\":1540587027064,\"previousClose\":25.98,\"change\":1.92,\"changePercent\":0.0739,\"iexMarketPercent\":null,\"iexVolume\":null,\"avgTotalVolume\":6348275,\"iexBidPrice\":null,\"iexBidSize\":null,\"iexAskPrice\":null,\"iexAskSize\":null,\"marketCap\":15815769757,\"peRatio\":99.64,\"week52High\":47.63,\"week52Low\":21.08,\"ytdChange\":-0.004395341922695778},{\"symbol\":\"GLUU\",\"companyName\":\"Glu Mobile Inc.\",\"primaryExchange\":\"Nasdaq Global Select\",\"sector\":\"Technology\",\"calculationPrice\":\"close\",\"open\":6.53,\"openTime\":1540560600386,\"close\":6.96,\"closeTime\":1540584000445,\"high\":7.205,\"low\":6.47,\"latestPrice\":6.96,\"latestSource\":\"Close\",\"latestTime\":\"October 26, 2018\",\"latestUpdate\":1540584000445,\"latestVolume\":9123436,\"iexRealtimePrice\":null,\"iexRealtimeSize\":null,\"iexLastUpdated\":null,\"delayedPrice\":6.96,\"delayedPriceTime\":1540584000445,\"extendedPrice\":7.04,\"extendedChange\":0.08,\"extendedChangePercent\":0.01149,\"extendedPriceTime\":1540587023995,\"previousClose\":6.42,\"change\":0.54,\"changePercent\":0.08411,\"iexMarketPercent\":null,\"iexVolume\":null,\"avgTotalVolume\":2759498,\"iexBidPrice\":null,\"iexBidSize\":null,\"iexAskPrice\":null,\"iexAskSize\":null,\"marketCap\":986958012,\"peRatio\":-21.75,\"week52High\":7.8,\"week52Low\":3.02,\"ytdChange\":1.0014653719008264},{\"symbol\":\"MLNX\",\"companyName\":\"Mellanox Technologies Ltd.\",\"primaryExchange\":\"Nasdaq Global Select\",\"sector\":\"Technology\",\"calculationPrice\":\"close\",\"open\":82,\"openTime\":1540560600183,\"close\":83.3,\"closeTime\":1540584000379,\"high\":86.07,\"low\":81.02,\"latestPrice\":83.3,\"latestSource\":\"Close\",\"latestTime\":\"October 26, 2018\",\"latestUpdate\":1540584000379,\"latestVolume\":3733341,\"iexRealtimePrice\":null,\"iexRealtimeSize\":null,\"iexLastUpdated\":null,\"delayedPrice\":83.3,\"delayedPriceTime\":1540584000379,\"extendedPrice\":83.3,\"extendedChange\":0,\"extendedChangePercent\":0,\"extendedPriceTime\":1540586431872,\"previousClose\":72.77,\"change\":10.53,\"changePercent\":0.1447,\"iexMarketPercent\":null,\"iexVolume\":null,\"avgTotalVolume\":629283,\"iexBidPrice\":null,\"iexBidSize\":null,\"iexAskPrice\":null,\"iexAskSize\":null,\"marketCap\":4412422491,\"peRatio\":26.28,\"week52High\":90.45,\"week52Low\":43.55,\"ytdChange\":0.4262384615384615},{\"symbol\":\"CIG\",\"companyName\":\"Comp En De Mn Cemig ADS American Depositary Shares\",\"primaryExchange\":\"New York Stock Exchange\",\"sector\":\"Utilities\",\"calculationPrice\":\"close\",\"open\":3.03,\"openTime\":1540560600201,\"close\":3.28,\"closeTime\":1540584121087,\"high\":3.29,\"low\":3.01,\"latestPrice\":3.28,\"latestSource\":\"Close\",\"latestTime\":\"October 26, 2018\",\"latestUpdate\":1540584121087,\"latestVolume\":24575213,\"iexRealtimePrice\":null,\"iexRealtimeSize\":null,\"iexLastUpdated\":null,\"delayedPrice\":3.28,\"delayedPriceTime\":1540584121087,\"extendedPrice\":3.29,\"extendedChange\":0.01,\"extendedChangePercent\":0.00305,\"extendedPriceTime\":1540587191647,\"previousClose\":2.99,\"change\":0.29,\"changePercent\":0.09699,\"iexMarketPercent\":null,\"iexVolume\":null,\"avgTotalVolume\":7959703,\"iexBidPrice\":null,\"iexBidSize\":null,\"iexAskPrice\":null,\"iexAskSize\":null,\"marketCap\":4784708531,\"peRatio\":14.26,\"week52High\":3.29,\"week52Low\":1.56,\"ytdChange\":0.7338799091725721},{\"symbol\":\"NATI\",\"companyName\":\"National Instruments Corporation\",\"primaryExchange\":\"Nasdaq Global Select\",\"sector\":\"Technology\",\"calculationPrice\":\"close\",\"open\":45.42,\"openTime\":1540560601566,\"close\":50.11,\"closeTime\":1540584000324,\"high\":50.41,\"low\":44.56,\"latestPrice\":50.11,\"latestSource\":\"Close\",\"latestTime\":\"October 26, 2018\",\"latestUpdate\":1540584000324,\"latestVolume\":2846618,\"iexRealtimePrice\":null,\"iexRealtimeSize\":null,\"iexLastUpdated\":null,\"delayedPrice\":50.11,\"delayedPriceTime\":1540584000324,\"extendedPrice\":49.413,\"extendedChange\":-0.697,\"extendedChangePercent\":-0.01391,\"extendedPriceTime\":1540584665684,\"previousClose\":42.89,\"change\":7.22,\"changePercent\":0.16834,\"iexMarketPercent\":null,\"iexVolume\":null,\"avgTotalVolume\":618465,\"iexBidPrice\":null,\"iexBidSize\":null,\"iexAskPrice\":null,\"iexAskSize\":null,\"marketCap\":6624948142,\"peRatio\":40.41,\"week52High\":53.57,\"week52Low\":38.78,\"ytdChange\":0.37159703020508916},{\"symbol\":\"BOOM\",\"companyName\":\"DMC Global Inc.\",\"primaryExchange\":\"Nasdaq Global Select\",\"sector\":\"Industrials\",\"calculationPrice\":\"close\",\"open\":38.25,\"openTime\":1540560600839,\"close\":37.64,\"closeTime\":1540584000355,\"high\":40.58,\"low\":34.39,\"latestPrice\":37.64,\"latestSource\":\"Close\",\"latestTime\":\"October 26, 2018\",\"latestUpdate\":1540584000355,\"latestVolume\":251730,\"iexRealtimePrice\":null,\"iexRealtimeSize\":null,\"iexLastUpdated\":null,\"delayedPrice\":37.64,\"delayedPriceTime\":1540584000355,\"extendedPrice\":37.64,\"extendedChange\":0,\"extendedChangePercent\":0,\"extendedPriceTime\":1540586434319,\"previousClose\":32.22,\"change\":5.42,\"changePercent\":0.16822,\"iexMarketPercent\":null,\"iexVolume\":null,\"avgTotalVolume\":115219,\"iexBidPrice\":null,\"iexBidSize\":null,\"iexAskPrice\":null,\"iexAskSize\":null,\"marketCap\":560781159,\"peRatio\":22.01,\"week52High\":51.05,\"week52Low\":18.56,\"ytdChange\":0.7008993792729954}]"

    @Test
    fun testGetStocks() {
        val sharedPrefs = Mockito.mock(SharedPreferences::class.java)
        val mockContext = Mockito.mock(Context::class.java)
        PreferenceService.defaultPreferences = sharedPrefs

        SyncService.preferenceService = PreferenceServiceMock()
        val stocks = SyncService.getStocks(mockContext)

        assertNotNull(stocks)
    }

    @Test fun testStocksReturnsListOfStocks() {
        val path = "stock/market/list/gainers/"
        val mockServer = MockWebServer()
        // Start the local server
        mockServer.start()

        // Get an okhttp client
        val okHttpClient = OkHttpClient.Builder()
                .build()

        val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("https://api.iextrading.com/1.0/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()

        var stockService = retrofit.create(StockNetworkAPI::class.java)
        // Initialized repository
        var stockRepoMock = StockRepoMock(stockService)

        val testObserver = TestObserver<List<Stock>>()

        // Mock a response with status 200 and sample JSON output
        val mockResponse = MockResponse()
                .setResponseCode(200)
                .setBody(mockJson)
        // Enqueue request
        mockServer.enqueue(mockResponse)

        // Call the API
        stockRepoMock.getStocks(path).subscribe(testObserver)
        testObserver.awaitTerminalEvent(2, TimeUnit.SECONDS)

        // No errors
        testObserver.assertNoErrors()
        // One list emitted
        testObserver.assertValueCount(1)
    }
}